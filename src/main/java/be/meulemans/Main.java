package be.meulemans;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.SecureRandom;
import java.time.Instant;

public class Main {

    public static void main(String[] args) throws IOException {
        String secret = TimedOneTimePassword.generateSecret(new SecureRandom());
        String secretKeyUri = TimedOneTimePassword.buildSecretKeyUri("testuser", secret, "anitjourney");
        byte[] qrCode = QrCodeUtils.generateQrCode(secretKeyUri, 200);

        Files.write(Paths.get("target/test-QrCodeUtils.png"), qrCode);
        TimedOneTimePassword totp = TimedOneTimePassword.fromSecret(secret);

        System.out.println("Secret is: " + secret);
        System.out.println("Secret key URI is: " + secretKeyUri);
        System.out.println("Value 123456 is valid now ? " + totp.isValid(123456, Instant.now()));
        System.out.println("QrCode generated and stored in file target/qr-generation/test-QrCodeUtils.png");
    }

}

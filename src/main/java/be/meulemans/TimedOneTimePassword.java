package be.meulemans;

import be.meulemans.common.UnexpectedServerException;
import org.apache.commons.codec.binary.Base32;
import org.apache.commons.codec.binary.BaseNCodec;

import javax.crypto.Mac;
import javax.crypto.ShortBufferException;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.time.Duration;
import java.time.Instant;

/**
 * Timed One Time Password: https://en.wikipedia.org/wiki/Time-based_One-time_Password_algorithm
 * Implementation of RFC 6238: https://tools.ietf.org/html/rfc6238
 * <p>
 * Based on one shared secret and the time, a token is 'generated' every 30 seconds.
 * <p>
 * Implementation inspired from:
 * - the reference implementation provided by Johan Rydell, PortWise, Inc.
 * - Jon Chambers https://github.com/jchambers/java-otp
 */
public class TimedOneTimePassword {

    private static final Duration stepDuration = Duration.ofSeconds(30);
    private static final int generatedOtpLength = 6;
    private static final String totpAlgorithm = "HmacSHA1";
    private static final int secretByteSize = 10;
    private static final int validationWindowSize = 5; // Number of previous and next totp that are accepted
    private static final BaseNCodec codec = new Base32();

    private final Mac mac;

    private TimedOneTimePassword(SecretKeySpec keySpec) {
        try {
            mac = Mac.getInstance(totpAlgorithm);
            mac.init(keySpec);
        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            throw new UnexpectedServerException("Unable to initialize TimedOneTimePassword", e);
        }
    }

    int generateValue(Instant instant) {
        return generateCounterBasedValue(timeBasedCounter(instant));
    }

    private long timeBasedCounter(Instant instant) {
        return instant.toEpochMilli() / stepDuration.toMillis();
    }

    int generateCounterBasedValue(long counter) {
        byte[] buffer = new byte[this.mac.getMacLength()];
        buffer[0] = (byte) ((counter & 0xff00000000000000L) >>> 56);
        buffer[1] = (byte) ((counter & 0x00ff000000000000L) >>> 48);
        buffer[2] = (byte) ((counter & 0x0000ff0000000000L) >>> 40);
        buffer[3] = (byte) ((counter & 0x000000ff00000000L) >>> 32);
        buffer[4] = (byte) ((counter & 0x00000000ff000000L) >>> 24);
        buffer[5] = (byte) ((counter & 0x0000000000ff0000L) >>> 16);
        buffer[6] = (byte) ((counter & 0x000000000000ff00L) >>> 8);
        buffer[7] = (byte) (counter & 0x00000000000000ffL);
        this.mac.update(buffer, 0, 8);

        try {
            this.mac.doFinal(buffer, 0);
        } catch (ShortBufferException e) {
            // We allocated the buffer to (at least) match the size of the MAC length at construction time, so this should never happen.
            throw new UnexpectedServerException("Error generating TOTP", e);
        }
        final int offset = buffer[buffer.length - 1] & 0x0f;

        return ((buffer[offset] & 0x7f) << 24 |
                (buffer[offset + 1] & 0xff) << 16 |
                (buffer[offset + 2] & 0xff) << 8 |
                (buffer[offset + 3] & 0xff)) %
                ((int) Math.pow(10, generatedOtpLength));
    }

    public boolean isValid(int value, Instant instant) {
        long counter = timeBasedCounter(instant);
        long startCounter = Math.max(0, counter - validationWindowSize);
        long endCounter = counter + validationWindowSize;

        for (long i = startCounter; i <= endCounter; i++) {
            if (generateCounterBasedValue(i) == value) {
                return true;
            }
        }
        return false;
    }

    public static TimedOneTimePassword fromSecret(String secret) {
        // See: https://issues.apache.org/jira/browse/CODEC-234
        // Commons Codec Base32::decode does not support lowercase letters.
        byte[] key = codec.decode(secret.toUpperCase());
        SecretKeySpec secretKeySpec = new SecretKeySpec(key, "RAW");
        return new TimedOneTimePassword(secretKeySpec);
    }

    public static String generateSecret(SecureRandom secureRandom) {
        byte[] buffer = new byte[secretByteSize];
        secureRandom.nextBytes(buffer);
        return codec.encodeToString(buffer);
    }

    public static String buildSecretKeyUri(String login, String secret, String issuer) {
        // https://github.com/google/google-authenticator/wiki/Key-Uri-Format
        // format is : otpauth://TYPE/LABEL?PARAMETERS
        // example: otpauth://totp/Example:alice@google.com?secret=JBSWY3DPEHPK3PXP&issuer=Example
        return String.format("otpauth://totp/%s:%s?secret=%s&issuer=%s&algorithm=SHA1", issuer, login, secret, issuer);
    }

}

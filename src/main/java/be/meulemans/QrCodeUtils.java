package be.meulemans;

import be.meulemans.common.UnexpectedServerException;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.EnumMap;

public final class QrCodeUtils {

    private QrCodeUtils() {
        throw new UnsupportedOperationException();
    }

    public static byte[] generateQrCode(String data, int size) {
        BitMatrix bitMatrix = generateQrCodeAsBitMatrix(data, size);
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            MatrixToImageWriter.writeToStream(bitMatrix, "png", baos);
            return baos.toByteArray();
        } catch (IOException e) {
            throw new UnexpectedServerException("Unable to convert QrCode as PNG", e);
        }
    }

    private static BitMatrix generateQrCodeAsBitMatrix(String data, int size) {
        EnumMap<EncodeHintType, Object> hints = new EnumMap<>(EncodeHintType.class);
        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.M);
        hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
        hints.put(EncodeHintType.MARGIN, 1);

        try {
            return new QRCodeWriter().encode(data, BarcodeFormat.QR_CODE, size, size, hints);
        } catch (WriterException e) {
            throw new UnexpectedServerException("Unable to generate QrCode", e);
        }
    }

}

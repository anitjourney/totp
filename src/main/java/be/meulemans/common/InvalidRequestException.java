package be.meulemans.common;


public class InvalidRequestException extends RuntimeException {

    private final ErrorCode errorCode;

    protected InvalidRequestException(ErrorCode errorCode, String message) {
        super(errorCode.name() + ": " + message);
        this.errorCode = errorCode;
    }

    public static InvalidRequestException forInvalidParameter(String details) {
        return new InvalidRequestException(ErrorCode.INVALID_PARAMETER, details);
    }

    public static InvalidRequestException forMissingParameter(String details) {
        return new InvalidRequestException(ErrorCode.MISSING_PARAMETER, details);
    }

    public static InvalidRequestException forInvalidTarget(String details) {
        return new InvalidRequestException(ErrorCode.INVALID_TARGET, details);
    }

    public static InvalidRequestException forMissingTarget(String details) {
        return new InvalidRequestException(ErrorCode.MISSING_TARGET, details);
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }

    public enum ErrorCode {
        INVALID_PARAMETER,
        MISSING_PARAMETER,
        INVALID_TARGET,
        MISSING_TARGET
    }

}

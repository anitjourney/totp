package be.meulemans.common;

/*
    Signals that an issue occurred during the processing of the a request by the services.
    The issue has an internal cause and is not due to an invalid request made by the client
    (InvalidRequestException should be thrown in those cases).
 */
public class UnexpectedServerException extends RuntimeException {

    public UnexpectedServerException(String message, Throwable cause) {
        super(message, cause);
    }

    public static UnexpectedServerException withNoRootCause(String details) {
        return new UnexpectedServerException(details, null);
    }
}

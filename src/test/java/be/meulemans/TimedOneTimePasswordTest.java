package be.meulemans;

import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.security.SecureRandom;
import java.time.Duration;
import java.time.Instant;

public class TimedOneTimePasswordTest {

    @Test
    public void testFromRFC() {
        // values from https://tools.ietf.org/html/rfc6238
        TimedOneTimePassword totp = TimedOneTimePassword.fromSecret("GEZDGNBVGY3TQOJQGEZDGNBVGY3TQOJQ");
        Assertions.assertThat(totp.generateValue(Instant.ofEpochSecond(59L))).isEqualTo(287082);
        Assertions.assertThat(totp.generateValue(Instant.ofEpochSecond(1111111109L))).isEqualTo(81804);
        Assertions.assertThat(totp.generateValue(Instant.ofEpochSecond(1111111111L))).isEqualTo(50471);
        Assertions.assertThat(totp.generateValue(Instant.ofEpochSecond(2000000000L))).isEqualTo(279037);
        Assertions.assertThat(totp.generateValue(Instant.ofEpochSecond(20000000000L))).isEqualTo(353130);
    }

    @Test
    public void testFromCounter() {
        TimedOneTimePassword totp = TimedOneTimePassword.fromSecret("GEZDGNBVGY3TQOJQGEZDGNBVGY3TQOJQ");
        Assertions.assertThat(totp.generateCounterBasedValue(0)).isEqualTo(755224);
        Assertions.assertThat(totp.generateCounterBasedValue(1)).isEqualTo(287082);
        Assertions.assertThat(totp.generateCounterBasedValue(2)).isEqualTo(359152);
        Assertions.assertThat(totp.generateCounterBasedValue(3)).isEqualTo(969429);
        Assertions.assertThat(totp.generateCounterBasedValue(4)).isEqualTo(338314);
        Assertions.assertThat(totp.generateCounterBasedValue(5)).isEqualTo(254676);
        Assertions.assertThat(totp.generateCounterBasedValue(6)).isEqualTo(287922);
        Assertions.assertThat(totp.generateCounterBasedValue(7)).isEqualTo(162583);
        Assertions.assertThat(totp.generateCounterBasedValue(8)).isEqualTo(399871);
        Assertions.assertThat(totp.generateCounterBasedValue(9)).isEqualTo(520489);
    }

    @Test
    public void testIsValid() {
        SecureRandom secureRandom = new SecureRandom();
        String secret = TimedOneTimePassword.generateSecret(secureRandom);

        Assertions.assertThat(secret).isNotBlank();

        TimedOneTimePassword totp = TimedOneTimePassword.fromSecret(secret);
        Instant now = Instant.now();
        int generatedValue = totp.generateValue(now);

        Assertions.assertThat(generatedValue).isNotZero().isPositive();
        Assertions.assertThat(totp.isValid(generatedValue, now)).isTrue();
        Assertions.assertThat(totp.isValid(generatedValue, now.minus(Duration.ofDays(1)))).isFalse();
        Assertions.assertThat(totp.isValid(generatedValue, now.plus(Duration.ofDays(1)))).isFalse();
        Assertions.assertThat(totp.isValid(generatedValue, now.plus(Duration.ofSeconds(42)))).isTrue();
        Assertions.assertThat(totp.isValid(generatedValue, now.minus(Duration.ofSeconds(42)))).isTrue();
    }

}

